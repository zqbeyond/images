## 参考

[JLink + GDB 调试方法](https://www.pianshen.com/article/2030363630/)

## 步骤

#### ① 连接jlink到目标板

#### ② 打开 J-Link GDB Server

![](https://note.youdao.com/yws/api/personal/file/WEB877db84e3ca5fe20839a9447fa9dc123?method=download&shareKey=e1a7bb316bcaccef23ca015e66c3bb84)

#### ③ 设置 J-Link GDB Server

jlink链接方式：USB

目标设备名字: ARM9(根据自己的板子修改)

![](https://note.youdao.com/yws/api/personal/file/WEB3d44a778659519aad33093aaa1def9f2?method=download&shareKey=856fba08241199ea1e8c7de1a39bbdf3)

#### ④ 链接 J-link


#### ⑤ 打开命令行，进入到镜像所在目录，执行命令

```
arm-none-eabi-gdb xxxx.elf
```

其中`arm-none-eabi-gdb`是GNU gdb工具，`xxxx.elf` 是待下载的文件


#### ⑥ 连接 GDB Server 的2331端口

```
tar ext:2331
```

> 注：此步以后就可以使用gdb的任何命令来调试程序了

#### ⑦ 设置断点

```
b main
```

#### ⑧ 运行程序

```
r
```


